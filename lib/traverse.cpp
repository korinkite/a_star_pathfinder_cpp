// IMPORT STANDARD LIBRARIES
#include <algorithm>
#include <iostream>
#include <cmath>

// IMPORT LOCAL LIBRARIES
#include "traverse.h"
#include "node.h"


constexpr int _START_VALUE = 3;
constexpr int _END_VALUE = 2;
constexpr int WALL_BLOCK = 1;


bool is_walkable(Node const &node, Graph const &graph)
{
    auto position = node.get_position();

    try {
        return graph[position[0]][position[1]] != WALL_BLOCK;
    }
    catch (...)
    {
        return false;
    }

    return true;
}


auto traverse::get_children(Node &node, Graph const &graph) -> traverse::Nodes
{
    auto position = node.get_position();
    auto row = position[0];
    auto column = position[1];

    auto row_count = graph.size();

    traverse::Nodes nodes {};

    for (int row_index = -1; row_index < 2; ++row_index)
    {
        auto row_position = row + row_index;
        if (row_position < 0 or row_position >= row_count)  // `row_position` is out of bounds
        {
            continue;
        }

        auto column_count = graph[row_position].size();

        for (int column_index = -1; column_index < 2; ++column_index)
        {
            auto column_position = column + column_index;
            if (column_position < 0 or column_position >= column_count)  // `column_position` is out of bounds
            {
                continue;
            }

            if (row_position == row && column_position == column)
            {
                // avoid returning `node` as a child of itself
                continue;
            }

            nodes.push_back({{row_position, column_position}, &node});
        }
    }

    return nodes;
}


auto get_item(Graph const &graph, traverse::PointCheckType const &predicate) -> Point
{
    auto row_count = graph.size();

    for (int row_index = 0; row_index < row_count; ++row_index)
    {
        auto const &row = graph[row_index];
        int row_size = graph[row_index].size();

        for (int column_index = 0; column_index < row_size; ++column_index)
        {
            if (predicate(row[column_index])) {
                return {row_index, column_index};
            }
        }
    }

    return {-1, -1};
}


auto traverse::get_end(Graph const &graph) -> Point
{
    traverse::PointCheckType const is_start_item = [](int item)
    {
        return item == _END_VALUE;
    };
    return get_item(graph, is_start_item);
}


int get_h(Node const &node, Node const &reference)
{
    auto node_position = node.get_position();
    auto reference_position = reference.get_position();

    return (pow(node_position[0] - reference_position[0], 2))
        + (pow(node_position[1] - reference_position[1], 2));
}


auto traverse::get_start(Graph const &graph) -> Point
{
    traverse::PointCheckType const is_start_item = [](int item)
    {
        return item == _START_VALUE;
    };
    return get_item(graph, is_start_item);
}


auto traverse::trace(Node &node) -> traverse::NodePath
{
    traverse::NodePath parents;
    Node* parent {node.get_parent()};

    while (parent)
    {
        parents.push_back(parent);
        parent = parent->get_parent();
    }

    delete parent;
    return parents;
}


auto traverse::a_star(Graph graph, Node user, Node goal) -> NodePath
{
    traverse::Nodes open {user};
    traverse::Nodes closed;

    traverse::Nodes::iterator open_iterator = open.begin();

    while (open_iterator != open.end())
    {
        auto current_node = open[0];
        int current_index = 0;

        for (traverse::Nodes::size_type index = 0, open_size = open.size(); index < open_size; ++index)
        {
            auto node = open[index];

            if (node.get_f() < current_node.get_f())
            {
                current_node = node;
                current_index = index;
            }
        }

        if (current_node == goal)
        {
            std::cout << "Goal found" << std::endl;

            goal.set_parent(&current_node);

            auto nodes = traverse::trace(goal);
            std::reverse(nodes.begin(), nodes.end());

            std::cout << "Found Nodes in traverse.cpp:\n";

            for (auto const *node : nodes)
            {
                std::cout << "    " << *node << "\n";
            }

            std::cout << std::endl;

            return nodes;
        }

        open_iterator = open.erase(open_iterator + current_index);
        closed.push_back(current_node);

        traverse::Nodes open_children {};

        for (auto &child : traverse::get_children(current_node, graph))
        {
            if (std::find(closed.begin(), closed.end(), child) != closed.end())
            {
                // It's already closed so don't add it again
                continue;
            }

            if (!is_walkable(child, graph))
            {
                continue;
            }

            child.set_g(child.get_parent()->get_g() + 1);
            child.set_h(get_h(child, goal));

            open_children.push_back(child);
        }

        open.reserve(open.size() + open_children.size());
        open.insert(open.end(), open_children.begin(), open_children.end());

        // XXX: Is there a way to NOT set the iterator to the beginning each time?
        open_iterator = open.begin();
    }

    // If this happens then that means the goal was never found
    return traverse::NodePath {};
}
