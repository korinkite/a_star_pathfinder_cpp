#pragma once

#include <ostream>
#include <vector>


using Graph = std::vector<std::vector<int>>;
using Point = std::vector<int>;


class Node {
    public:
        Node() {};
        Node(Point position) : position(position) {};
        Node(Point position, Node *parent) : position(position) { this->parent = new Node(*parent); }
        /* Node(Point position, int g) : position(position), g(g) {}; */
        /* Node(Point position, int g, int h) : position(position), g(g), h(h) {}; */
        /* Node(Point position, int g, int h, Node *parent) : position(position), g(g), h(h) { this->parent = parent; } */

        auto get_f() const -> int { return this->f; };
        auto get_g() const -> int { return this->g; };
        auto get_h() const -> int { return this->h; };
        Node* get_parent() { return this->parent; };
        auto get_position() const -> Point const { return this->position; };

        void set_g(int g) { this->g = g; this->recalculate_f(); };
        void set_h(int h) { this->h = h; this->recalculate_f(); };
        void set_parent(Node* parent) { this->parent = parent; };

        bool operator==(Node const other) { return this->get_position() == other.get_position(); };
        Node* parent = NULL;

    protected:
        void recalculate_f() { this->f = this->get_g() + this->get_h(); };

    private:
        Point position;
        int g = 0;
        int h = 0;
        int f = 0;

        friend std::ostream& operator<<(std::ostream &stream, Node const &node);
};
