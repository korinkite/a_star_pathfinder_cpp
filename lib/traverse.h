#pragma once

// IMPORT STANDARD LIBRARIES
#include <functional>
#include <memory>
#include <vector>

// IMPORT LOCAL LIBRARIES
#include "node.h"


namespace traverse {
    using Nodes = std::vector<Node>;
    using NodePath = std::vector<Node*>;
    /* using NodePath = std::vector<std::unique_ptr<Node>>; */
    using PointCheckType = std::function<bool(Graph::size_type)>;

    auto get_children(Node &node, Graph const &graph) -> Nodes;

    auto get_end(Graph const &graph) -> Point;

    auto get_start(Graph const &graph) -> Point;

    auto trace(Node &node) -> NodePath;

    /* auto a_star(Graph const &graph, Node const &user, Node &goal) -> NodePath; */

    auto a_star(Graph graph, Node user, Node goal) -> NodePath;
};
