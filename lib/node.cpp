#include "node.h"


std::ostream& operator<<(std::ostream& stream, Node const &node) {
    /* stream << "Node(" << node.position << node.g << ", " << node.h; */
    stream << "Node(" << "{" << node.position[0] << ", " << node.position[1] << "}";
    stream << ", " << node.g << ", " << node.h;

    if (node.parent != NULL)
    {
        stream << ", " << node.parent;
    }
    else
    {
        stream << ", NULL";
    }

    stream << ")";

    return stream;
}
