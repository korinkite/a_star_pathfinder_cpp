// IMPORT STANDARD LIBRARIES
#include <iostream>

// IMPORT LOCAL LIBRARIES
#include "traverse.h"
#include "node.h"


int main() {
    Graph graph {
        {0, 0, 0, 0, 1, 0, 0},
        {0, 3, 0, 0, 1, 0, 2},
        {0, 0, 0, 0, 1, 0, 0},
        {0, 0, 0, 0, 1, 0, 0},
        {0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 1, 0, 0},
        {0, 0, 0, 0, 1, 0, 0},
    };

    Node user {traverse::get_start(graph)};
    Node goal {traverse::get_end(graph)};

    std::cout << "Found user " << user << std::endl;
    std::cout << "Found end " << goal << std::endl;

    auto nodes = traverse::a_star(graph, user, goal);

    std::cout << "Found Nodes in main.cpp:\n";

    for (auto const *node : nodes)
    {
        std::cout << "    " << *node << "\n";
    }

    std::cout << std::endl;

    return 0;
}
